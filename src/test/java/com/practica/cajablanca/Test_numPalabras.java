package com.practica.cajablanca;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;


public class Test_numPalabras {
	 Editor editor = new Editor();
	@Test
	public void Path1(){
		editor.leerFichero("texto6lineas.txt");
		assertThrows(java.lang.IllegalArgumentException.class,()->{editor.numPalabras(0, 6, "sit");});
	}
	@Test
	public void Path2(){
		editor.leerFichero("texto6lineas.txt");
		assertThrows(java.lang.IllegalArgumentException.class,()->{editor.numPalabras(1, 9, "sit");});
	}
	@Test
	public void Path3(){
		assertEquals(0,editor.numPalabras(1, -1, "sit"));
	}
	@Test
	public void Path4(){
		editor.leerFichero("texto6lineas.txt");
		assertEquals(0,editor.numPalabras(7, 6, "consequat"));
	}
	
}
