package com.practica.cajablanca;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.cajanegra.EmptyCollectionException;

public class Test_sustituirPalabra {
	Editor editor = new Editor();
	Editor aux = new Editor();
	@Test
	public void Path1() throws EmptyCollectionException {
		editor.sustituirPalabra("Lorem","Carlos" );
		assertTrue(editor.equals(aux));
	}
	@Test
	public void Path2() throws EmptyCollectionException {
		editor.leerFichero("lineaVacia.txt");
		aux.leerFichero("lineaVacia.txt");
		editor.sustituirPalabra("Lorem","Carlos" );
		assertTrue(editor.equals(aux));
	}
	
	@Test
	public void Path3() throws EmptyCollectionException{
		editor.leerFichero("Lorem.txt");
		editor.sustituirPalabra("Lorem","Carlos" );
		aux.leerFichero("Carlos.txt");
		assertTrue(editor.equals(aux));
	}
	@Test
	public void Path4() throws EmptyCollectionException {
		editor.leerFichero("Lorem.txt");
		editor.sustituirPalabra("Pablo","Carlos");
		aux.leerFichero("Lorem.txt");
		assertTrue(editor.equals(aux));
	}
	@Test
	public void Path5() throws EmptyCollectionException {
		editor.leerFichero("dosLineasVacias.txt");
		editor.sustituirPalabra("Pablo","Carlos");
		aux.leerFichero("dosLineasVacias.txt");
		assertTrue(editor.equals(aux));
	}
	
}
