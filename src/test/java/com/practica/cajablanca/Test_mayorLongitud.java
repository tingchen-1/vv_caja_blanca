package com.practica.cajablanca;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.cajanegra.EmptyCollectionException;

public class Test_mayorLongitud {
	Editor editor = new Editor();
	String palabra;
	String mayor;
	@Test
	public void Path1() throws EmptyCollectionException {
		mayor = editor.mayorLongitud();
		assertEquals(palabra,mayor);
	}
	@Test
	public void Path3() throws EmptyCollectionException {
		editor.leerFichero("lineaVacia.txt");
		mayor = editor.mayorLongitud();
		palabra = new String();
		assertEquals(palabra,mayor);
	}
	@Test
	public void Path4() throws EmptyCollectionException {
		editor.leerFichero("Lorem.txt");
		mayor = editor.mayorLongitud();
		palabra = "Lorem";
		assertEquals(palabra,mayor);
	}
}
